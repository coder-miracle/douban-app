import 'package:url_launcher/url_launcher.dart';

/// 尝试用自带浏览器打开URL
launchUrl(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
