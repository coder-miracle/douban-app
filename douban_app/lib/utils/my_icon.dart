import 'package:flutter/material.dart';

class MyIcons {
  // 豆瓣猜
  static const IconData doubanCai = const IconData(
    0xe8a1,
    fontFamily: 'myIcon',
    matchTextDirection: true
  );
  // 书单
  static const IconData bookList = const IconData(
    0xe62c,
    fontFamily: 'myIcon',
    matchTextDirection: true
  );
  // 查找书籍
  static const IconData searchBook = const IconData(
    0xe6a0,
    fontFamily: 'myIcon',
    matchTextDirection: true
  );
  // 榜单
  static const IconData rankingList = const IconData(
    0xe6af,
    fontFamily: 'myIcon',
    matchTextDirection: true
  );
}