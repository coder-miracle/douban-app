import 'package:douban_app/views/book/index.dart';
import 'package:douban_app/views/home/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '豆瓣',
      theme: ThemeData(
          primaryColor: Colors.white,
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent),
      home: MyStackPage(),
    );
  }
}

class MyStackPage extends StatefulWidget {
  @override
  _MyStackPageState createState() => _MyStackPageState();
}

class _MyStackPageState extends State<MyStackPage> {
  int _currentIndex = 1;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
        BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
            maxHeight: MediaQuery.of(context).size.height),
        designSize: Size(360, 690),
        context: context,
        orientation: Orientation.portrait);

    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          currentIndex: _currentIndex,
          selectedItemColor: Colors.green,
          selectedFontSize: 14,
          unselectedFontSize: 14,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home),
                activeIcon: Icon(
                  Icons.home,
                  color: Colors.green,
                ),
                label: "首页"),
            BottomNavigationBarItem(
                icon: Icon(Icons.book),
                activeIcon: Icon(
                  Icons.book,
                  color: Colors.green,
                ),
                label: "书影音"),
            BottomNavigationBarItem(
                icon: Icon(Icons.group_work),
                activeIcon: Icon(
                  Icons.group_work,
                  color: Colors.green,
                ),
                label: "小组"),
            BottomNavigationBarItem(
                icon: Icon(Icons.shop),
                activeIcon: Icon(
                  Icons.shop,
                  color: Colors.green,
                ),
                label: "市场"),
            BottomNavigationBarItem(
                icon: Icon(Icons.person),
                activeIcon: Icon(
                  Icons.person,
                  color: Colors.green,
                ),
                label: "我的"),
          ],
          onTap: (index) {
            setState(() {
              print("index %d"+index.toString()+"");
              _currentIndex = index;
            });
          }),
      body: IndexedStack(
        index: _currentIndex,
        children: <Widget>[Home(), Book(),Home(),Home(), Home()],
      ),
    );
  }
}
