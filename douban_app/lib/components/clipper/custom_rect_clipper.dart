import 'package:flutter/cupertino.dart';

/// 自定义矩形裁剪框，通过传入Rect来进行裁剪
class CustomRectClipper extends CustomClipper<Rect> {
  Rect rect;
  CustomRectClipper({this.rect});
  @override
  Rect getClip(Size size) {
    return rect ?? Rect.fromLTWH(0, 0, size.width, size.height);
  }

  @override
  bool shouldReclip(covariant CustomClipper<Rect> oldClipper) {
    return false;
  }
}
