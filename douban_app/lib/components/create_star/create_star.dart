import 'package:flutter/material.dart';

class CreateStar extends StatefulWidget {
  final double rating;
  final double maxRating;
  final double starSize;
  final int count;

  CreateStar(
      {@required double rating,
      this.maxRating = 10,
      this.starSize = 30,
      this.count = 5})
      : rating = rating > 10 ? 10 : rating;

  @override
  _CreateStarState createState() => _CreateStarState();
}

class _CreateStarState extends State<CreateStar> {
//  未选中星星
  List<Widget> getUncheckedStar() {
    List<Widget> resultStar = [];
    List.generate(
        widget.count,
        (index) => {
              resultStar.add(Icon(
                Icons.star,
                color: Colors.grey,
                size: widget.starSize,
              ))
            });
    return resultStar;
  }

  List<Widget> getCheckedStar() {
    //    计算
    double leftWidth = 10;
    double oneValue = widget.maxRating / widget.count;
    double finalStar = widget.rating % oneValue;
    double finalStarRate = finalStar / oneValue;
    int hasFullNum = (widget.rating / oneValue).floor();
    leftWidth = finalStarRate * widget.starSize;

    List<Widget> checkedFullStar = [];
    List.generate(
        hasFullNum,
        (index) => {
              checkedFullStar.add(Icon(
                Icons.star,
                size: widget.starSize,
                color: Colors.orange,
              ))
            });

    Widget leftStar = ClipRect(
      clipper: ClipStar(width: leftWidth),
      child: Icon(Icons.star, size: widget.starSize, color: Colors.orange),
    );
    checkedFullStar.add(leftStar);
    return checkedFullStar;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Row(
          children: getUncheckedStar(),
          mainAxisSize: MainAxisSize.min,
        ),
        Row(
          children: getCheckedStar(),
          mainAxisSize: MainAxisSize.min,
        ),
      ],
    );
  }
}

class ClipStar extends CustomClipper<Rect> {
  final double width;
  ClipStar({this.width});
  @override
  getClip(Size size) {
    return Rect.fromLTRB(0, 0, width, size.height);
  }

  @override
  bool shouldReclip(ClipStar oldClipper) {
    return width != oldClipper.width;
  }
}
