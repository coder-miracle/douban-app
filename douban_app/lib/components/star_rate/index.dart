import 'package:douban_app/components/clipper/custom_rect_clipper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 五角星评级组件
/// 可指定五角星宽度，间距，以及填充色，背景色
/// 可指定 星星数
///
class StartRate extends StatefulWidget {
  final double rate;
  final double totalScore;
  final double starWidth;
  final double starSpacing;
  final Color starFrontColor;
  final Color starBgColor;
  final int starCount;
  final bool showLabel;
  StartRate(
      {Key key,
      this.rate,
      this.totalScore = 10,
      this.starWidth = 11,
      this.starSpacing = -1.5,
      this.starCount = 5,
      this.showLabel = true,
      this.starFrontColor = const Color.fromRGBO(254, 153, 1, 1),
      this.starBgColor = const Color.fromRGBO(224, 224, 224, 1)})
      : super(key: key ?? UniqueKey()); // 务必需要保证有key,防止不更新

  @override
  _StartRateState createState() => _StartRateState();
}

class _StartRateState extends State<StartRate> {
  _buildStarList({bool needCliped = false, Color color}) {
    double rate = widget.rate;
    int starCount = widget.starCount;
    double starSpacing = widget.starSpacing;
    double starWidth = widget.starWidth;
    double totalScore = widget.totalScore;

    double width = starCount * starWidth + starSpacing * (starCount - 1);
    double clipWidth = (rate / totalScore) * width;

    List<Widget> iconList = [];
    for (int i = 0; i < starCount; i++) {
      iconList.add(Positioned(
        left: i > 0 ? i * (starWidth.w + starSpacing.w) : 0.0,
        child: Icon(
          Icons.star,
          color: color,
          size: starWidth.w,
        ),
      ));
    }
    var starWidget = Container(
        width: width + 5.0.w,
        height: starWidth,
        child: Stack(
          children: iconList,
        ));
    if (!needCliped) {
      return starWidget;
    }

    return Container(
      child: ClipRect(
        child: starWidget,
        clipper: CustomRectClipper(
            rect: Rect.fromLTWH(0, 0, clipWidth.w, starWidth)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var stars = Container(
      child: Stack(
        children: [
          _buildStarList(color: widget.starBgColor),
          _buildStarList(needCliped: true, color: widget.starFrontColor)
        ],
      ),
    );

    if (!widget.showLabel) {
      return stars;
    }

    return Row(
      children: [
        stars,
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 3.w),
          child: Text(
            (widget.rate ?? 0.0).toString(),
            style: TextStyle(
                color: widget.starFrontColor,
                fontSize: 10.6.sp,
                fontWeight: FontWeight.w500),
          ),
        )
      ],
    );
  }
}
