import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MoreButton extends StatelessWidget {
  final double height;
  final Function onPressed;
  const MoreButton({Key key, this.height, @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onPressed,
        child: Container(
          height: height ?? 30.w,
          width: 40.w,
          padding: EdgeInsets.symmetric(horizontal: 13.w),
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6.w),
              color: Color.fromRGBO(242, 242, 242, 1)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "更多",
                style: TextStyle(
                    fontSize: 12.sp, color: Color.fromRGBO(127, 127, 127, 1)),
              ),
              Container(
                width: 18.sp,
                height: 18.sp,
                child: Stack(children: [
                  Positioned(
                      left: -4.w,
                      child: Icon(
                        Icons.chevron_right_outlined,
                        color: Color.fromRGBO(127, 127, 127, 1),
                        size: 18.sp,
                      )),
                ]),
              )
            ],
          ),
        ));
  }
}
