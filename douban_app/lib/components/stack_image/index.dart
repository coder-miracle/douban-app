import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class StackImage extends StatelessWidget {
  final double containerWidth;
  final double imageWidth;
  final double imageHeight;
  final List<String> imgUrls;
  final bool sameHeight;

  const StackImage(this.imgUrls,
      {Key key,
      this.containerWidth = 56.6,
      this.imageWidth,
      this.imageHeight,
      this.sameHeight = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _imageWidth = imageWidth ?? 0.1.sw;
    double _imageHeight = imageHeight ?? 40.w;
    int len = imgUrls.length;
    List<Widget> children = [];
    int realLen = min(len - 1, 2);
    for (int i = realLen; i >= 0; i--) {
      children.add(Positioned(
          right: (realLen - i) * (containerWidth - _imageWidth) / 2,
          bottom: 0,
          child: Container(
            width: _imageWidth,
            height: sameHeight ? _imageHeight : _imageHeight * (1 - i * 0.06),
            decoration: BoxDecoration(
                boxShadow: [BoxShadow(color: Colors.grey[200], blurRadius: 5)],
                borderRadius: BorderRadius.circular(6.w),
                image: DecorationImage(
                    image: NetworkImage(imgUrls[i]), fit: BoxFit.cover)),
          )));
    }
    return Stack(children: children);
  }
}
