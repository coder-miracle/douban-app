import 'package:flutter/material.dart';

class TopSearch extends StatelessWidget {
  // const TopSearch({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          flex: 0,
          child: Icon(
            Icons.dehaze,
            color: Colors.green,
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            width: 280,
            height: 32,
            margin: EdgeInsets.only(left: 12, right: 12),
            padding: EdgeInsets.only(top: 14),
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 238, 244, 240),
              borderRadius: BorderRadius.circular(12),
            ),
            child: TextField(
              keyboardType: TextInputType.text,
              cursorColor: Colors.grey[350],
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: '国产剧小组',
                  hintStyle: TextStyle(
                      fontSize: 14.0, color: Color.fromRGBO(152, 164, 154, 1))),
            ),
          ),
        ),
        Expanded(
          flex: 0,
          child: Icon(
            Icons.mail,
            color: Colors.green,
          ),
        )
      ],
    );
  }
}
