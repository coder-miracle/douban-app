import 'package:flutter/material.dart';

class ComomonPageScaffold extends StatelessWidget {
  // 后续功能待开发...

  final String titleName;
  final Widget body;
  ComomonPageScaffold({Key key, this.titleName, this.body}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
            titleName,
            style: TextStyle(fontSize: 18),
          ),
          elevation: 0,
          backgroundColor: Colors.white,
          foregroundColor: Color(0xFF333333)),
      backgroundColor: Colors.white,
      body: body,
    );
  }
}
