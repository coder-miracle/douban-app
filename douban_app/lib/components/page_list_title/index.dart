import 'package:flutter/material.dart';

class PageListTitle extends StatelessWidget {
  final String mainTitle;
  final String rightText;
  final bool canJump;
  final Widget jumpWidget;

  PageListTitle(
      {this.mainTitle = '标题',
      this.rightText = '描述',
      this.canJump = false,
      this.jumpWidget = null}) {}

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          this.mainTitle,
          style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
        ),
        this.canJump ? _Jumpable(context) : _NotJump()
      ],
    );
  }

  Widget _NotJump() {
    return Row(
      children: [
        Text(
          this.rightText,
          style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
        ),
        Icon(Icons.chevron_right)
      ],
    );
  }

  Widget _Jumpable(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return this.jumpWidget;
        }));
      },
      child: Row(
        children: [
          Text(
            this.rightText,
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
          ),
          Icon(Icons.chevron_right)
        ],
      ),
    );
  }
}
