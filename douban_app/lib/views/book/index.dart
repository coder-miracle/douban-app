import 'package:douban_app/components/round_cap_underline_tab_indicator.dart';
import 'package:douban_app/views/book/tab_pages/tv/index.dart';
import 'package:douban_app/components/top_search/index.dart';
import 'package:douban_app/views/book/tab_pages/movie/index.dart';
import 'package:douban_app/views/book/tab_pages/read/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Book extends StatefulWidget {
  @override
  _BookState createState() => _BookState();
}

class _BookState extends State<Book> with SingleTickerProviderStateMixin {
  TabController _tabController;
  ScrollController _containerScrollController = ScrollController();

  List tabs = ["电影", "电视", "读书", "原创小说", "音乐", "同城"];
  int curItem = 0;
  double containerTop = 0; // 整体容器的top

  void _scrollContainer(val) {
    _containerScrollController.jumpTo(val);
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: tabs.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          title: TopSearch(),
        ),
        body: ListView(controller: _containerScrollController, physics: NeverScrollableScrollPhysics(), children: [
          Container(
            height: 42.0,
            decoration: BoxDecoration(color: Colors.white),
            child: TabBar(
              isScrollable: true,
              controller: this._tabController,
              // indicatorColor: Color.fromARGB(255, 25, 25, 25),
              // indicatorWeight: 2,
              indicatorSize: TabBarIndicatorSize.label,
              // 定制tab下划线，圆角
              indicator: RoundCapUnderlineTabIndicator(
                  borderSide: BorderSide(width: 3.w, color: Colors.black)),
              labelColor: Color.fromARGB(255, 25, 25, 25),
              unselectedLabelColor: Color.fromARGB(255, 129, 129, 129),
              onTap: (index) {
                setState(() => curItem = index);
              },
              tabs: this
                  .tabs
                  .map((item) => Tab(
                        child: Text(
                          item,
                          style: TextStyle(fontSize: 12.5.sp),
                        ),
                      ))
                  .toList(),
            ),
          ),
          Container(
              height: 1.sh,
              decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(
                          width: 1.w, color: Color.fromRGBO(224, 224, 224, 1))),
                  color: Colors.white),
              child: TabBarView(controller: this._tabController, children: [
                TabMovie(
                  onChangeContainerScrollOffset: (val) {
                    _scrollContainer(val);
                  },
                ),
                TV(),
                TabRead(),
                Container(),
                TabMovie(),
                Container()
              ]))
        ]));
  }

  @override
  void dispose() {
    // 释放资源
    _tabController.dispose();
    super.dispose();
  }
}
