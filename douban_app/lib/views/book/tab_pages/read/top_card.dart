import 'package:douban_app/views/book/tab_pages/read/find_books/index.dart';
import 'package:flutter/material.dart';

Widget topCard(IconData icon, String text, BuildContext context) {
  return GestureDetector(
    onTap: () {
      switch(text) {
        case '找图书':
          Navigator.push(context, 
          MaterialPageRoute(
            builder: (context) {
              return FindBooksByType();
            }
          ));
          break;
        default:
          break;
      }
    },
    child: Container(
      width: 80,
      padding: EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
          color: Colors.white,
          border:
              Border.all(width: 1, color: Color.fromARGB(255, 233, 233, 233)),
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(color: Color.fromARGB(255, 244, 244, 244), blurRadius: 5)
          ]),
      child: Column(
        children: [
          Icon(
            icon,
            color: Color.fromARGB(255, 73, 73, 73),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            text,
            style: TextStyle(
                color: Color.fromARGB(255, 129, 129, 129), fontSize: 12),
          )
        ],
      ),
    ),
  );
}
