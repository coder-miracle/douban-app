import 'package:douban_app/models/book_av/book_rank_card.dart';
import 'package:flutter/material.dart';

import 'book_card_list.dart';

class RankList extends StatelessWidget {
  final BookRankCard bookRankCard;
  RankList(this.bookRankCard);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 310,
      height: 370,
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: NetworkImage(bookRankCard.backgroundImageUrl)
        ),
        borderRadius: BorderRadius.circular(10)
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(bookRankCard.mainTitle,style: TextStyle(fontSize: 20, color: Colors.white),),
              Text(bookRankCard.subTitle, style: TextStyle(fontSize: 12, color: Colors.white),),
            ],
          ),
          SizedBox(height: 10),
          Container(
            child: Column(
              children: bookRankCard.rankList.map((e) => BookCardListItem(e)).toList()
            ),
          )
        ],
      ),
    );
  }
}

