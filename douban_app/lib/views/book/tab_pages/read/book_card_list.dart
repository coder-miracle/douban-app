import 'package:douban_app/components/create_star/create_star.dart';
import 'package:douban_app/models/book_av/rank_list.dart';
import 'package:flutter/material.dart';

class BookCardListItem extends StatefulWidget {

  final RankList _rankList;
  BookCardListItem(this._rankList);

  @override
  _BookCardListItemState createState() => _BookCardListItemState();
}

class _BookCardListItemState extends State<BookCardListItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 6),
      child: IntrinsicHeight(
        child: Row(
          children: [
            Container(
              width: 24,
              margin: EdgeInsets.only(right: 10),
              child: Center(
                child: Text(widget._rankList.index, style: TextStyle(color: Colors.white, fontSize: 18),),
              ),
            ),
            Container(
              width: 32,
              height: 44,
              margin: EdgeInsets.only(right: 10),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(widget._rankList.bookCoverUrl)
                )
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget._rankList.bookName, style: TextStyle(color: Colors.white, fontSize: 16),),
                  SizedBox(height: 6,),
                  CreateStar(rating: widget._rankList.ranking,starSize: 16,)
                ],
              ),
            )
          ],
        ),
      )
    );
  }
}
