import 'package:douban_app/models/book_av/new_book.dart';
import 'package:flutter/material.dart';

Widget bookCard(NewBook book) {

  return Container(
    width: 290,
    margin: EdgeInsets.only(right: 10),
    padding: EdgeInsets.all(12),
    decoration: BoxDecoration(
      border: Border.all(width: 1, color: Color(0xFFE0E0E0)),
      borderRadius: BorderRadius.circular(12)
    ),
    child: Row(
      children: [
        Container(
          width: 98,
          height: 140,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(book.coverUrl),
              fit: BoxFit.cover
            ),
            borderRadius: BorderRadius.circular(10),
          ),
        ),
        SizedBox(width: 10,),
        Expanded(
          child: Container(
            child: Column(
              children: [
                Text(book.bookName, style: TextStyle(color: Color(0xFF191919), fontSize: 16, height: 1),),
                Text('${book.like}人想读/${book.author}', style: TextStyle(color: Color(0xFF818181), fontSize: 12, height: 1.6)),
                Row(
                  children: book.tags.map((e) => _tag(e)).toList(),
                ),
                Container(
                  margin: EdgeInsets.only(top: 6),
                  padding: EdgeInsets.all(6),
                  decoration: BoxDecoration(
                    color: Color(0xFFF7F7F7),
                    borderRadius: BorderRadius.circular(6)
                  ),
                  child: Text(book.describe,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(
                      fontSize: 12
                    ),
                  )
                )
              ],
            ),
          ),
        )
      ],
    ),
  );
}

Widget _tag(String text) {
  return Container(
    margin: EdgeInsets.only(top: 10, right: 6),
    padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
    decoration: BoxDecoration(
      color: Color(0xFFF7F7F7),
      borderRadius: BorderRadius.circular(10)
    ),
    child: Text(text, style: TextStyle(fontSize: 12),),
  );
}