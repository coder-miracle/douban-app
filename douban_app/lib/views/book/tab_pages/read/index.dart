
import 'package:douban_app/components/page_list_title/index.dart';
import 'package:douban_app/models/book_av/book_rank_card.dart';
import 'package:douban_app/models/book_av/new_book.dart';
import 'package:douban_app/service/http_book_av.dart';
import 'package:douban_app/utils/my_icon.dart';
import 'package:douban_app/views/book/tab_pages/read/book_card.dart';
import 'package:douban_app/views/book/tab_pages/read/find_books/index.dart';
import 'package:douban_app/views/book/tab_pages/read/rank_list_card.dart';
import 'package:douban_app/views/book/tab_pages/read/top_card.dart';
import 'package:douban_app/views/web_view/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TabRead extends StatefulWidget {
  @override
  _TabReadState createState() => _TabReadState();
}

const List<Map<String, dynamic>> mockData = [
  {
    'bookName': '霸王别姬',
    'coverUrl': 'https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561716440.jpg',
    'like': 8757,
    'author': '[日]上野千鹤子',
    'tags': ['女性主义', '上野千鹤子'],
    'describe': '女性主义是什么？为什么我们需要女性主义。如何正确'
  },
  {
    'bookName': '美丽人生',
    'coverUrl': 'https://img2.doubanio.com/view/photo/s_ratio_poster/public/p2578474613.jpg',
    'like': 8757,
    'author': '[日]上野千鹤子',
    'tags': ['女性主义', '上野千鹤子'],
    'describe': '女性主义是什么？为什么我们需要女性主义。如何正确'
  }
];

const List<Map<String, dynamic>> doubanRankMockData = [
  {
    'mainTitle': '一周热门图书榜',
    'subTitle': '豆瓣榜单',
    'backgroundImageUrl': 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG211320865019.jpg',
    'rankList': [
      {
        'index': '1',
        'bookCoverUrl': 'https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561716440.jpg',
        'bookName': '霸王别姬',
        'ranking': 9.0
      },
      {
        'index': '2',
        'bookCoverUrl': 'https://img9.doubanio.com/view/subject/s/public/s33958826.jpg',
        'bookName': '拯救计划',
        'ranking': 6.0
      },
      {
        'index': '3',
        'bookCoverUrl': 'https://img1.doubanio.com/view/subject/s/public/s34003939.jpg',
        'bookName': '电子游戏微历史',
        'ranking': 8.0
      },
    ]
  },
  {
    'mainTitle': '豆瓣读书排行',
    'subTitle': '豆瓣榜单',
    'backgroundImageUrl': 'https://tenfei05.cfp.cn/creative/vcg/800/new/VCG41160533664.jpg',
    'rankList': [
      {
        'index': '1',
        'bookCoverUrl': 'https://img3.doubanio.com/view/subject/s/public/s33974760.jpg',
        'bookName': '诸子百家',
        'ranking': 9.0
      },
      {
        'index': '2',
        'bookCoverUrl': 'https://img2.doubanio.com/view/subject/s/public/s33979743.jpg',
        'bookName': '电力商人',
        'ranking': 8.0
      },
      {
        'index': '3',
        'bookCoverUrl': 'https://img2.doubanio.com/view/subject/s/public/s34005071.jpg',
        'bookName': '再见妖精',
        'ranking': 8.0
      },
    ]
  },
];

class _TabReadState extends State<TabRead> {
  
  List<NewBook> books = [];
  List<BookRankCard> bookRankCard = [];

  // 实例化请求
  BookAvRequest bookAvRequest = BookAvRequest();

  @override
  void initState() {
    super.initState();
    bookAvRequest.getMovieTopList(1, 10);

    // 初始化模型数据
    for (var item in mockData) {
      books.add(NewBook.fromJson(item));
    }
    for (var item in doubanRankMockData) {
      bookRankCard.add(BookRankCard.fromJson(item));
    }
  }

  String currentTabItem = '全部';
  List<String> bookTypeList = ['全部', '文学', '小说', '历史文化', '社会纪实', '科学新知', '游戏'];

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                topCard(MyIcons.searchBook, '找图书', context),
                topCard(MyIcons.rankingList, '豆瓣榜单', context),
                topCard(MyIcons.doubanCai, '豆瓣猜', context),
                topCard(MyIcons.bookList, '豆瓣书单', context),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 10),
            child: Column(
              children: [
                PageListTitle(
                  mainTitle: '新书速递',
                  rightText: '全部 200',
                  canJump: true,
                  jumpWidget: FindBooksByType(),
                ),
                Container(
                  height: 50,
                  child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: bookTypeList.map((text) => bookTypeBtn(text)).toList()
                  ),
                ),
                Container(
                  height: 150,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: books.length,
                    itemBuilder: (ctx, index) {
                      return bookCard(books[index]);
                    },
                  ),
                )
              ],
            )
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('豆瓣榜单', style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, height: 2)),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  height: 250,
                  child: ListView.separated(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (ctx, index) {
                        return RankList(bookRankCard[index]);
                      }, 
                      separatorBuilder: (ctx, index) {
                        return VerticalDivider(
                          width: 10,
                        );
                      }, 
                      itemCount: bookRankCard.length
                    )
                )
              ],
            )
          )
        ],
      ),
    );
  }

  // 新书速递分类按钮
  Widget bookTypeBtn(String text) {
    Color defaultBackgroundColor = Color.fromARGB(255, 247, 247, 247);
    Color defaultFontColor = Color.fromARGB(255, 129, 129, 129);
    Color highlightBackgroundColor = Color(0xFFE6F8EA);
    Color highlightFontColor = Color(0xFF139A34);
    return TextButton(
      onPressed: () {
        setState(() {
          currentTabItem = text;
        });
        Navigator.push(context, MaterialPageRoute(
          builder: (context) {
            return WebViewPage();
          }
        ));
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 4, horizontal: 10),
        decoration: BoxDecoration(
            color: currentTabItem == text ? highlightBackgroundColor : defaultBackgroundColor,
            borderRadius: BorderRadius.circular(8)
        ),
        child: Text(text, style: TextStyle(color: currentTabItem == text ? highlightFontColor : defaultFontColor)),
      ),
    );
  }
}

