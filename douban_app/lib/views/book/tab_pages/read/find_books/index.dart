import 'package:douban_app/components/common_page_scaffold/index.dart';
import 'package:douban_app/models/book_av/book_type_list.dart';
import 'package:douban_app/models/book_av/sub_type_list.dart';
import 'package:flutter/material.dart';

class FindBooksByType extends StatefulWidget {
  @override
  _FindBooksByTypeState createState() => _FindBooksByTypeState();
}

class _FindBooksByTypeState extends State<FindBooksByType> {
  List<Map<String, dynamic>> bookTypeListMock = [
    {
      "typeTitle": "专题",
      "subTypeList": [
        {"subTypeTitle": "影视原著", "color": 0xFF473334},
        {"subTypeTitle": "名著经典", "color": 0xFF86635E},
      ]
    },
    {
      "typeTitle": "精神避难所",
      "subTypeList": [
        {"subTypeTitle": "文学", "color": 0xFF834139},
        {"subTypeTitle": "诗歌", "color": 0xFF7D828B},
        {"subTypeTitle": "小说", "color": 0xFF88443A},
        {"subTypeTitle": "散文", "color": 0xFF866E52},
        {"subTypeTitle": "戏剧", "color": 0xFF86635E},
        {"subTypeTitle": "戏剧", "color": 0xFF8A7250},
        {"subTypeTitle": "传记", "color": 0xFF86635E},
        {"subTypeTitle": "哲学", "color": 0xFF212850},
        {"subTypeTitle": "历史", "color": 0xFF86746C},
      ]
    },
    {
      "typeTitle": "生活启示录",
      "subTypeList": [
        {"subTypeTitle": "心理", "color": 0xFF8B8383},
        {"subTypeTitle": "教育", "color": 0xFF171514},
        {"subTypeTitle": "生活", "color": 0xFF443644},
        {"subTypeTitle": "摄影", "color": 0xFF414642},
        {"subTypeTitle": "绘画", "color": 0xFF828B80},
        {"subTypeTitle": "语言", "color": 0xFF27314F},
        {"subTypeTitle": "旅游", "color": 0xFF774465},
        {"subTypeTitle": "家居", "color": 0xFF8B8B85},
        {"subTypeTitle": "美食", "color": 0xFF2A3B47},
      ]
    },
  ];

  List<BookTypeList> bookTypeList = [];
  List<Widget> dataList = [];

  @override
  void initState() {
    super.initState();
    for (var item in bookTypeListMock) {
      bookTypeList.add(BookTypeList.fromJson(item));
    }
    dataList = bookTypeList.map((item) => _TypeWrap(item)).toList();
    dataList.insert(0, _TopBlock());
    print(dataList);
  }

  @override
  Widget build(BuildContext context) {
    return ComomonPageScaffold(
      titleName: '分类找图书',
      body: Container(
          padding: EdgeInsets.all(20), child: ListView(children: dataList)),
    );
  }

  Widget _TopBlock() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      height: 50,
      decoration: BoxDecoration(
          border: Border.all(width: 1, color: Color(0xFFDFDFDF)),
          borderRadius: BorderRadius.circular(8)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Icon(Icons.filter_list),
              Text(
                '分类找图书',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ],
          ),
          Row(
            children: [
              Text(
                '地区、类型、特色',
                style: TextStyle(color: Color(0xFF818181)),
              ),
              Icon(
                Icons.chevron_right_rounded,
                color: Color(0xFF1A1A1A),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _TypeWrap(BookTypeList bookType) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            bookType.typeTitle,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 18,
          ),
          Wrap(
              spacing: 10.0,
              runSpacing: 10.0,
              children:
                  bookType.subTypeList.map((item) => _TypeCard(item)).toList())
        ],
      ),
    );
  }

  Widget _TypeCard(SubTypeList subType) {
    return Container(
        width: 110,
        height: 60,
        decoration: BoxDecoration(
            color: Color(subType.color),
            borderRadius: BorderRadius.circular(10)),
        child: Center(
          child: Text(
            subType.subTypeTitle,
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ));
  }
}
