import 'package:douban_app/views/book/tab_pages/movie/coming_soon/coming_soon_card.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/cupertino.dart';

class ComingSoonList extends StatefulWidget {
  ComingSoonList({Key key}) : super(key: key);

  @override
  _ComingSoonListState createState() => _ComingSoonListState();
}

class _ComingSoonListState extends State<ComingSoonList> {
  int internalCount = 8;
  int internationalCount = 10;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ComingSoonCard(
            cardTitle: "国内即将上映",
            cardSubTitle: "近期有${internalCount}部热门电影",
            imgUrlList: [
              "https://img2.doubanio.com/view/photo/s_ratio_poster/public/p2692433481.webp",
              "https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2684036997.webp",
              "https://img9.doubanio.com/view/photo/s_ratio_poster/public/p2700138245.webp"
            ],
            onTapFunc: () {
              print("tap 国内即将上映");
            },
          ),
          ComingSoonCard(
            cardTitle: "全球值得期待",
            cardSubTitle: "近期有${internationalCount}部热门电影",
            imgUrlList: [
              "https://img2.doubanio.com/view/photo/s_ratio_poster/public/p2692433481.webp",
              "https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2684036997.webp",
              "https://img9.doubanio.com/view/photo/s_ratio_poster/public/p2700138245.webp"
            ],
            onTapFunc: () {
              print("tap 全球值得期待");
            },
          )
        ],
      ),
    );
  }
}
