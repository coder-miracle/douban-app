import 'dart:math';

import 'package:douban_app/components/stack_image/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

typedef _OnTapFunc = void Function();

class ComingSoonCard extends StatelessWidget {
  final String cardTitle;
  final String cardSubTitle;
  final List<String> imgUrlList;
  final _OnTapFunc onTapFunc;

  const ComingSoonCard(
      {Key key,
      this.cardTitle,
      this.cardSubTitle,
      this.imgUrlList = const [],
      this.onTapFunc})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var cardWidth = 0.42.sw;
    var paddingWidth = 10.w;
    var halfContainerWidth = cardWidth / 2 - 16.w - paddingWidth;
    var imageWidth = 0.1.sw;
    double imageHeight = halfContainerWidth - 4.w;
    return InkWell(
      onTap: () {
        this.onTapFunc();
      },
      child: Container(
        margin: EdgeInsets.only(top: 16.w),
        width: cardWidth,
        height: 98.w,
        padding: EdgeInsets.symmetric(vertical: paddingWidth),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(6.w),
            border: Border.all(color: Color.fromRGBO(223, 223, 223, 1)),
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(253, 253, 253, 1),
                offset: Offset.zero,
                blurRadius: 8.w,
              )
            ]),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: paddingWidth, right: 4.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text(cardTitle), Icon(Icons.chevron_right_outlined)],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 4.w),
              padding: EdgeInsets.symmetric(horizontal: paddingWidth),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: halfContainerWidth + 6.w,
                    height: imageHeight,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(cardSubTitle,
                            softWrap: true, style: TextStyle(fontSize: 10.sp))
                      ],
                    ),
                  ),
                  Container(
                    width: halfContainerWidth,
                    height: imageHeight,
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                        color: Colors.grey[200],
                        offset: Offset.zero,
                        blurRadius: 8.w,
                      )
                    ]),
                    child: StackImage(imgUrlList,
                        containerWidth: halfContainerWidth,
                        imageWidth: imageWidth,
                        imageHeight: imageHeight),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
