import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BannerItem {
  String title;
  IconData icon;
  BannerItem({
    this.title = "默认",
    this.icon = Icons.check_box_outline_blank,
  });
}

List<Widget> _getBanner(List<BannerItem> itemList) {
  return itemList
      .map((item) => DecoratedBox(
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Color.fromRGBO(250, 250, 250, 1),
                    offset: Offset(2.0, 2.0),
                    blurRadius: 4.0.w)
              ],
              border: Border.all(
                color: Color.fromRGBO(223, 223, 223, 1),
                width: 1.w,
              ),
              borderRadius: BorderRadius.all(Radius.circular(6.r))),
          child: InkWell(
            onTap: () => {
              //TODO: 点击跳转事件
              print("tap")
            },
            child: Container(
              width: 0.18.sw,
              padding: EdgeInsets.symmetric(vertical: 4.w),
              child: Column(
                children: [
                  Icon(
                    item.icon,
                    color: Colors.black54,
                  ),
                  Text(
                    item.title,
                    style: TextStyle(
                      fontSize: 10.sp,
                      color: Colors.black54,
                    ),
                  )
                ],
              ),
            ),
          )))
      .toList();
}

Widget TopBanner() {
  var children = _getBanner(
    [
      BannerItem(icon: Icons.search, title: '找电影'),
      BannerItem(icon: Icons.bar_chart, title: '豆瓣榜单'),
      BannerItem(icon: Icons.schedule, title: '即将上映'),
      BannerItem(icon: Icons.video_library, title: '豆瓣片单')
    ],
  );
  return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 14.w),
      width: 1.sw,
      child: Wrap(alignment: WrapAlignment.spaceBetween, children: children));
}
