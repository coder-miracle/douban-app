import 'package:douban_app/components/star_rate/index.dart';
import 'package:douban_app/models/book_av/movie_overview_card_model.dart';
import 'package:douban_app/models/book_av/movie_ranking_item_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

typedef _CallbackFunc = Function(MovieOverviewCardModel movieRankingItemModel);

class MovieOverviewListCard extends StatelessWidget {
  final MovieOverviewCardModel movieOverviewCardModel;
  final double cardHeight;
  final _CallbackFunc onPress;
  const MovieOverviewListCard(
      {Key key,
      @required this.movieOverviewCardModel,
      this.cardHeight = 220,
      this.onPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double radius = 12.w;
    return InkWell(
      onTap: () {
        if (this.onPress != null) {
          this.onPress(this.movieOverviewCardModel);
        }
      },
      child: Container(
          width: 0.7.sw,
          margin: EdgeInsets.only(right: 10.w),
          child: Stack(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(radius),

                /// TODO:增加空白图
                child: Image.network(
                  movieOverviewCardModel.bgImageUrl,
                  width: 0.7.sw,
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                  width: 0.7.sw,
                  height: cardHeight,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: [0.0, 0.45, 0.6],
                        colors: [0.1, 0.98, 1.0].map((opacity) {
                          return movieOverviewCardModel.maskColor
                              .withOpacity(opacity);
                        }).toList()),
                    borderRadius: BorderRadius.all(Radius.circular(radius)),
                  )),
              Container(
                  width: 0.7.sw,
                  height: cardHeight,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: [0, 0.45, 1],
                        colors: [0.4, 0.1, 0.0].map((opacity) {
                          return Colors.black.withOpacity(opacity);
                        }).toList()),
                    borderRadius: BorderRadius.all(Radius.circular(radius)),
                  )),
              Container(
                child: Column(
                  children: [
                    Container(
                        padding: EdgeInsets.only(
                            left: 16.w, right: 16.w, top: 11.w, bottom: 16.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              movieOverviewCardModel.title,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.sp,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Text(movieOverviewCardModel.subTitle,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 10.sp,
                                ))
                          ],
                        )),
                    Container(
                      padding: EdgeInsets.only(
                        left: 14.w,
                        right: 16.w,
                      ),
                      child: Column(
                        children: _buildOverviewCardInnerList(
                            movieOverviewCardModel.movieRakingItem),
                      ),
                    )
                  ],
                ),
              )
            ],
          )),
    );
  }

  List<Widget> _buildOverviewCardInnerList(List<MovieRankingItemModel> items) {
    int index = 0;
    double itemHeight = 48.w;
    return items.map((item) {
      return Container(
        height: itemHeight,
        padding: EdgeInsets.only(bottom: 10.w),
        child: Row(
          children: [
            Container(
                width: 22.w,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      (++index).toString(),
                      style: TextStyle(
                          fontSize: 12.sp,
                          color: Colors.white,
                          fontWeight: FontWeight.w600),
                    ),
                    item.rankingChange != null
                        ? Padding(
                            padding: EdgeInsets.only(top: 4.w),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  item.rankingChange == 0
                                      ? '-'
                                      : item.rankingChange > 0
                                          ? "↑"
                                          : "↓",
                                  style: TextStyle(
                                      fontSize: item.rankingChange == 0
                                          ? 10.sp
                                          : 5.6.sp,
                                      color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: item.rankingChange == 0 ? 0 : 2.w),
                                  child: Text(
                                    item.rankingChange == 0
                                        ? ''
                                        : item.rankingChange.abs().toString(),
                                    style: TextStyle(
                                        fontSize: 9.sp, color: Colors.white70),
                                  ),
                                )
                              ],
                            ),
                          )
                        : Container()
                  ],
                )),
            Container(
              padding: EdgeInsets.only(left: 14.w, right: 10.w),
              child: Container(
                height: itemHeight,
                width: itemHeight / 1.6,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6.w),
                    image: DecorationImage(
                        image: NetworkImage(item.imgUrl), fit: BoxFit.cover)),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  item.name,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: 12.5.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.w500),
                ),
                Container(
                  margin: EdgeInsets.only(top: 4.w, bottom: 2.w),
                  child: StartRate(
                    rate: item.rate,
                    starBgColor: Color.fromRGBO(0, 0, 0, 0.22),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }).toList();
  }
}
