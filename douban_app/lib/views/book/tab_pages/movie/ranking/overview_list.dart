import 'dart:math';

import 'package:douban_app/components/more_button/index.dart';
import 'package:douban_app/models/book_av/movie_overview_card_model.dart';
import 'package:douban_app/views/book/tab_pages/movie/ranking/overview_list_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/cupertino.dart';

class MovieOverviewList extends StatefulWidget {
  final List<MovieOverviewCardModel> movieOverviewCardList;

  const MovieOverviewList({Key key, this.movieOverviewCardList})
      : super(key: key);

  @override
  _MovieOverviewListState createState() => _MovieOverviewListState();
}

class _MovieOverviewListState extends State<MovieOverviewList> {
  List<MovieOverviewCardModel> movieOverviewCardList;

  @override
  void initState() {
    super.initState();
    movieOverviewCardList = this.movieOverviewCardList ??
        [
          {
            "title": "实时热门电影",
            "subTitle": "豆瓣榜单",
            "bgImageUrl":
                "https://img3.doubanio.com/view/photo/photo/public/p2669288400.jpg",
            "maskColor": "0xff57607F",
            "detailUrl": "https://www.baidu.com",
            "movieRakingItem": [
              {
                "name": "007: 无暇赴死",
                "ranking": 1,
                "rankingChange": 1,
                "imgUrl":
                    "https://img9.doubanio.com/view/photo/m_ratio_poster/public/p2707553644.jpg",
                "rate": 7.0
              },
              {
                "name": "007: 无暇赴死",
                "ranking": 2,
                "rankingChange": -1,
                "imgUrl":
                    "https://img9.doubanio.com/view/photo/m_ratio_poster/public/p2707553644.jpg",
                "rate": 7.0
              },
              {
                "name": "007: 无暇赴死",
                "ranking": 3,
                "rankingChange": 0,
                "imgUrl":
                    "https://img9.doubanio.com/view/photo/m_ratio_poster/public/p2707553644.jpg",
                "rate": 7.0
              }
            ]
          },
          {
            "title": "一周口碑电影榜",
            "subTitle": "豆瓣榜单",
            "bgImageUrl":
                "https://img3.doubanio.com/view/photo/photo/public/p2669288400.jpg",
            "maskColor": "0xff7f2234",
            "movieRakingItem": [
              {
                "name": "无暇赴死3",
                "ranking": 1,
                "rankingChange": 1,
                "imgUrl":
                    "https://img9.doubanio.com/view/photo/m_ratio_poster/public/p2707553644.jpg",
                "rate": 7.0
              },
              {
                "name": "无暇赴死4",
                "ranking": 2,
                "rankingChange": 1,
                "imgUrl":
                    "https://img9.doubanio.com/view/photo/m_ratio_poster/public/p2707553644.jpg",
                "rate": 6.0
              },
              {
                "name": "无暇赴死5",
                "ranking": 3,
                "rankingChange": 1,
                "imgUrl":
                    "https://img9.doubanio.com/view/photo/m_ratio_poster/public/p2707553644.jpg",
                "rate": 7.9
              }
            ]
          },
          {
            "title": "豆瓣电影 Top250",
            "subTitle": "豆瓣榜单",
            "bgImageUrl":
                "https://img3.doubanio.com/view/photo/photo/public/p2669288400.jpg",
            "maskColor": "0xff341918",
            "movieRakingItem": [
              {
                "name": "肖申克的救赎",
                "ranking": 1,
                // "rankingChange": 1,
                "imgUrl":
                    "https://img9.doubanio.com/view/photo/m_ratio_poster/public/p2707553644.jpg",
                "rate": 9.7
              },
              {
                "name": "肖申克的救赎",
                "ranking": 2,
                // "rankingChange": 0,
                "imgUrl":
                    "https://img9.doubanio.com/view/photo/m_ratio_poster/public/p2707553644.jpg",
                "rate": 9.7
              },
              {
                "name": "肖申克的救赎",
                "ranking": 2,
                // "rankingChange": 0,
                "imgUrl":
                    "https://img9.doubanio.com/view/photo/m_ratio_poster/public/p2707553644.jpg",
                "rate": 9.7
              },
            ]
          }
        ].map((e) {
          return MovieOverviewCardModel.fromMap(e);
        }).toList();
  }

  @override
  Widget build(BuildContext context) {
    int widgetCount = min(movieOverviewCardList.length, 3) + 1;
    double height = 200.w;
    return Container(
      height: height,
      margin: EdgeInsets.only(left: 20.w, right: 20.w, top: 28.w, bottom: 32.w),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: widgetCount,
          itemBuilder: (buildContext, index) {
            /// 电影榜单.片单概览
            if (index < widgetCount - 1) {
              return MovieOverviewListCard(
                  onPress: (movieOverviewCardModel) {
                    //TODO:
                    print("跳转至 ${movieOverviewCardModel.title}的详情页");
                  },
                  cardHeight: height,
                  movieOverviewCardModel: movieOverviewCardList[index]);
            }

            /// 更多按钮
            return MoreButton(
              height: height,
              onPressed: () {
                //TODO:
                print("跳转至更多电影榜单");
              },
            );
          }),
    );
  }
}
