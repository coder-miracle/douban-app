import 'dart:async';

import 'package:douban_app/utils/launch_url.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:url_launcher/url_launcher.dart';

class ADArea extends StatefulWidget {
  ADArea({Key key}) : super(key: key);

  @override
  _ADAreaState createState() => _ADAreaState();
}

class _ADAreaState extends State<ADArea> {
  String imgUrl = "";
  String jumpUrl = "";

  @override
  void initState() {
    super.initState();

    imgUrl =
        "http://5b0988e595225.cdn.sohucs.com/images/20180221/f81bcc41e8d245c1a8ef5c44426af2fb.jpeg";
    jumpUrl = "http://flutter.dev";
  }

  @override
  Widget build(BuildContext context) {
    // 显示弹窗
    Future<void> _showMyDialog() async {
      return showDialog<void>(
        context: context,
        // barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('广告弹窗'),
            content: SingleChildScrollView(
              child: ListBody(
                children: const <Widget>[
                  Text('这是一条广告.'),
                  Text('是否跳转广告商品主页?'),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('取消'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: const Text('确定'),
                onPressed: () {
                  // 关闭弹窗，并跳转浏览器
                  Navigator.of(context).pop();
                  launchUrl(jumpUrl);
                },
              ),
            ],
          );
        },
      );
    }

    return InkWell(
      onTap: _showMyDialog,
      child: Container(
          margin:
              EdgeInsets.only(left: 20.w, right: 20.w, top: 32.w, bottom: 20.w),
          decoration: BoxDecoration(
              color: Colors.green.shade400,
              image: DecorationImage(
                  fit: BoxFit.cover, image: NetworkImage(imgUrl)),
              borderRadius: BorderRadius.all(Radius.circular(6.w))),
          width: 1.sw,
          height: 98.w,
          child: Stack(
            children: [
              Positioned(
                bottom: 8.0.w,
                right: 8.0.w,
                child: Container(
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(0, 0, 0, 0.4),
                      borderRadius: BorderRadius.circular(4.w)),
                  padding: EdgeInsets.only(
                      left: 6.w, top: 1.w, right: 6.w, bottom: 3.w),
                  child: Text(
                    "广告",
                    style: TextStyle(color: Colors.white54, fontSize: 11.sp),
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
