import 'dart:math';

import 'package:douban_app/components/more_button/index.dart';
import 'package:douban_app/models/book_av/movie_play_list_card_model.dart';
import 'package:douban_app/views/book/tab_pages/movie/playlist/card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PlayList extends StatefulWidget {
  PlayList({Key key}) : super(key: key);

  @override
  _PlayListState createState() => _PlayListState();
}

class _PlayListState extends State<PlayList> {
  List<MoviePlayListCardModel> modelList;
  @override
  void initState() {
    super.initState();

    modelList = [
      {
        'cardTitle': "007系列",
        'total': 25,
        'focusCount': 4278,
        'coverImgList': [
          'https://img2.doubanio.com/view/photo/s_ratio_poster/public/p2678111601.jpg',
          'https://img9.doubanio.com/view/photo/s_ratio_poster/public/p2633129235.jpg',
          'https://img9.doubanio.com/view/photo/s_ratio_poster/public/p2697816056.jpg'
        ],
      },
      {
        'cardTitle': "高分穿越电影榜高分穿越电影榜高分穿越电影榜",
        'total': 20,
        'focusCount': 2937,
        'coverImgList': [
          'https://img2.doubanio.com/view/photo/s_ratio_poster/public/p2678111601.jpg',
          'https://img9.doubanio.com/view/photo/s_ratio_poster/public/p2633129235.jpg',
          'https://img9.doubanio.com/view/photo/s_ratio_poster/public/p2697816056.jpg'
        ],
      }
    ].map((e) {
      return MoviePlayListCardModel.fromMap(e);
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    int widgetCount = min(modelList.length, 3) + 1;
    double height = 98.w;
    return Container(
      height: height,
      width: 1.sw,
      margin: EdgeInsets.only(left: 20.w, right: 20.w, bottom: 20.w),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: widgetCount,
          itemBuilder: (context, index) {
            if (index < widgetCount - 1) {
              return PlayListCard(
                moviePlayListCardModel: modelList[index],
                height: height,
                onPressed: (item) {
                  print("准备跳转${item.cardTitle}详情");
                },
              );
            }
            return MoreButton(
                height: height,
                onPressed: () {
                  print("查看更多影视片单");
                });
          }),
    );
  }
}
