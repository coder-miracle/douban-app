import 'package:douban_app/components/stack_image/index.dart';
import 'package:douban_app/models/book_av/movie_play_list_card_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

typedef _CallBackFunc = void Function(MoviePlayListCardModel);

class PlayListCard extends StatelessWidget {
  final MoviePlayListCardModel moviePlayListCardModel;
  final _CallBackFunc onPressed;
  final double height;
  const PlayListCard(
      {Key key,
      @required this.moviePlayListCardModel,
      this.onPressed,
      this.height = 120})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var cardWidth = 0.7.sw;
    var padding = 12.w;
    var imageHeight = (height - padding * 2);
    var imageWidth = imageHeight * 0.75;
    return InkWell(
      onTap: () {
        onPressed(moviePlayListCardModel);
      },
      child: Container(
        width: cardWidth,
        margin: EdgeInsets.only(right: 12.w),
        padding: EdgeInsets.all(padding),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.w),
            color: Color.fromRGBO(238, 236, 237, 1)),
        child: Row(
          children: [
            Container(
                width: imageHeight,
                height: imageHeight,
                margin: EdgeInsets.only(right: 14.w),
                child: Stack(
                  children: [
                    StackImage(
                      moviePlayListCardModel.coverImgList,
                      containerWidth: imageHeight,
                      imageWidth: imageWidth,
                      imageHeight: imageHeight,
                      sameHeight: false,
                    ),
                    Container(
                        child: Stack(
                      children: [
                        Positioned(
                            bottom: 0,
                            left: 0,
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(0, 0, 0, 0.6),
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(6.w),
                                      topRight: Radius.circular(6.w))),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 6.5.w, vertical: 2.w),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: 2.w),
                                    child: Icon(
                                      Icons.video_collection_outlined,
                                      color: Colors.white,
                                      size: 6.6.sp,
                                    ),
                                  ),
                                  Text(
                                    "片单",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 8.sp),
                                  )
                                ],
                              ),
                            )),
                      ],
                    ))
                  ],
                )),
            Expanded(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  moviePlayListCardModel.cardTitle,
                  maxLines: 2,
                  style: TextStyle(
                      fontSize: 14.sp, overflow: TextOverflow.ellipsis),
                ),
                Text(
                  "共${moviePlayListCardModel.total}部  ${moviePlayListCardModel.focusCount}人关注",
                  style: TextStyle(
                      color: Color.fromRGBO(131, 131, 131, 1), fontSize: 12.w),
                ),
              ],
            ))
          ],
        ),
      ),
    );
  }
}
