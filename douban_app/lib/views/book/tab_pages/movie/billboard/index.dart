import 'package:douban_app/utils/launch_url.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 用于电影页的宣传栏

class Billboard extends StatefulWidget {
  Billboard({Key key}) : super(key: key);

  @override
  _BillboardState createState() => _BillboardState();
}

class _BillboardState extends State<Billboard> {
  String imgUrl = "";
  String jumpUrl = "";

  @override
  void initState() {
    super.initState();

    imgUrl =
        "http://img2.baidu.com/it/u=2063801547,2313500636&fm=253&app=138&f=JPG?w=1920&h=500";
    jumpUrl = "http://flutter.dev";
  }

  // 显示弹窗
  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      // barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('测试推广'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text('这是一条简单的推广.'),
                Text('是否跳转至推广链接？'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('取消'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('确定'),
              onPressed: () {
                // 关闭弹窗，并跳转浏览器
                Navigator.of(context).pop();
                launchUrl(jumpUrl);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: _showMyDialog,
        child: Container(
          margin:
              EdgeInsets.only(left: 20.w, right: 20.w, top: 30.w, bottom: 24.w),
          decoration: BoxDecoration(
              color: Colors.green.shade400,
              image: DecorationImage(
                  image: NetworkImage(imgUrl), fit: BoxFit.cover),
              borderRadius: BorderRadius.all(Radius.circular(6.w))),
          width: 1.sw,
          height: 98.w,
        ));
  }
}
