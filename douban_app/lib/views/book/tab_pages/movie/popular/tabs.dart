import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

typedef _TabChangeFunc = void Function(int);

class CusTabs extends StatefulWidget {
  // 切换tab的回调
  final _TabChangeFunc onTabChange;
  // 默认激活的tab
  final int defaultActiveIndex;
  // tab的list
  final List<String> labelList;

  CusTabs(
      {this.onTabChange,
      this.defaultActiveIndex = 0,
      this.labelList = const ["默认标题"]});

  @override
  _CusTabsState createState() => _CusTabsState();
}

class _CusTabsState extends State<CusTabs> {
  int activeIndex;
  @override
  void initState() {
    super.initState();
    activeIndex = widget.defaultActiveIndex;
  }

  _buildTabs() {
    List<Widget> tabs = [];
    double fontHeight = 18.sp;
    var labelList = widget.labelList;
    for (int i = 0; i < labelList.length; i++) {
      //title
      tabs.add(InkWell(
          onTap: () {
            // 修改状态，重新渲染
            setState(() => activeIndex = i);
            // 调用回调函数
            if (widget.onTabChange != null) {
              widget.onTabChange(activeIndex);
            }
          },
          child: Text(
            labelList[i],
            style: activeIndex == i
                ? TextStyle(fontSize: fontHeight, fontWeight: FontWeight.w600)
                : TextStyle(fontSize: fontHeight, color: Colors.grey),
          )));

      // 加分隔线
      if (i + 1 < labelList.length) {
        tabs.add(Container(
          margin: EdgeInsets.symmetric(horizontal: 4.w),
          height: fontHeight,
          child: VerticalDivider(
            color: Colors.grey,
            width: 1.w,
          ),
        ));
      }
    }
    return tabs;
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        flex: 0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: _buildTabs(),
        ));
  }
}
