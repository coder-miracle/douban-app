import 'package:douban_app/views/book/tab_pages/movie/popular/tabs.dart';
import 'package:douban_app/views/book/tab_pages/movie/popular/video_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/cupertino.dart';

class PopularMovie extends StatefulWidget {
  @override
  _PopularMovieState createState() => _PopularMovieState();
}

class _PopularMovieState extends State<PopularMovie> {
  int allCount = 28;
  int activeIndex = 0;

  var hotShowData = [
    {"name": '很长的电影名称，要多长有多长', "rate": 7.2},
    {"name": '热映名称1', "rate": 9.0},
    {"name": '热映名称2'},
    {"name": '热映名称3'}
  ];

  var popularData = [
    {"name": '热门很长的电影名称，要多长有多长', "rate": 8.2},
    {"name": '热门电影名称1', "rate": 4.2},
    {"name": '热门电影名称2'},
    {"name": '热门电影名称3'}
  ];
  var activeList;

  @override
  void initState() {
    super.initState();
    activeList = activeIndex == 0 ? hotShowData : popularData;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CusTabs(
                defaultActiveIndex: activeIndex,
                onTabChange: (int index) {
                  setState(() {
                    activeIndex = index;
                    activeList = activeIndex == 0 ? hotShowData : popularData;
                  });
                },
                labelList: ["影院热映", "豆瓣热门"],
              ),
              TextButton(
                onPressed: () => setState(() {
                  print("TODO: 跳转 tabIndex 为 ${activeIndex} 的详情");
                }),
                child: Row(
                  children: [
                    Text(
                      "全部 ${allCount}",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    Icon(Icons.chevron_right_outlined, color: Colors.black)
                  ],
                ),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 10.w),
          child: VideoList(activeList),
        )
      ],
    ));
  }
}
