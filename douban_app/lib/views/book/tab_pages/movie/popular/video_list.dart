import 'package:douban_app/models/book_av/movie_base_model.dart';
import 'package:douban_app/views/book/tab_pages/movie/popular/video_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class VideoList extends StatefulWidget {
  final List<Map<String, dynamic>> listMap;
  VideoList(this.listMap, {Key key}) : super(key: key);

  @override
  _VideoListState createState() => _VideoListState();
}

class _VideoListState extends State<VideoList> {
  /// 将map转化为VideoCardModel list
  getMovieList(List<Map<String, dynamic>> listMap, {int limit = 10}) {
    return listMap.sublist(0, listMap.length > limit ? limit : null).map((e) {
      return VideoCard(videoCardModel: MovieBaseModel.fromMap(e));
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180.w,
      margin: EdgeInsets.symmetric(horizontal: 20.w),
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: getMovieList(widget.listMap),
      ),
    );
  }
}
