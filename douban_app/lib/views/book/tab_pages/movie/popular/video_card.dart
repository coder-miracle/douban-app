import 'package:douban_app/components/star_rate/index.dart';
import 'package:douban_app/models/book_av/movie_base_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class VideoCard extends StatefulWidget {
  final MovieBaseModel videoCardModel;
  final double containerWidth;
  VideoCard(
      {Key key, @required this.videoCardModel, this.containerWidth = 0.24})
      : super(key: key);

  @override
  _VideoCardState createState() => _VideoCardState();
}

class _VideoCardState extends State<VideoCard> {
  @override
  Widget build(BuildContext context) {
    var videoCardModel = widget.videoCardModel;
    var width = widget.containerWidth;
    return Container(
      margin: EdgeInsets.only(right: 10.w),
      width: width.sw,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(6.w),
            child: Image.network(
              videoCardModel.imgUrl ??
                  "https://img9.doubanio.com/view/photo/s_ratio_poster/public/p2687443734.webp",
              height: 0.355.sw,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 3.w),
            child: Text(
              videoCardModel.name,
              softWrap: true,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 11.sp),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 3.w),
            child: Row(
              children: [
                StartRate(
                  rate: videoCardModel.rate ?? 0.0,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
