import 'dart:math';

import 'package:douban_app/views/book/tab_pages/movie/ad_area.dart';
import 'package:douban_app/views/book/tab_pages/movie/billboard/index.dart';
import 'package:douban_app/views/book/tab_pages/movie/coming_soon/index.dart';
import 'package:douban_app/views/book/tab_pages/movie/playlist/index.dart';
import 'package:douban_app/views/book/tab_pages/movie/popular/index.dart';
import 'package:douban_app/views/book/tab_pages/movie/ranking/overview_list.dart';
import 'package:douban_app/views/book/tab_pages/movie/top_banner.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

typedef _CallbackFunc = Function(double dy);

class TabMovie extends StatefulWidget {
  final _CallbackFunc onChangeContainerScrollOffset;

  TabMovie({Key key, this.onChangeContainerScrollOffset}) : super(key: key);

  @override
  _TabMovieState createState() => _TabMovieState();
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({
    @required this.minHeight,
    @required this.maxHeight,
    @required this.child,
  });

  final double minHeight;
  final double maxHeight;
  final Widget child;

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => max(maxHeight, minHeight);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}

class _TabMovieState extends State<TabMovie> {
  ScrollController _controller;
  final GlobalKey fixHeaderKey = GlobalKey();
  final GlobalKey containerKey = GlobalKey();

  double maxScrollHeight = 42.0; // 最大滚动高度，与 tabbar的高度保持一致
  double minScrollHeight = 0.0;
  double offsetY = 0.0;
  double lastOffsetY = 0.0;

  @override
  void initState() {
    _controller = ScrollController();

    _controller.addListener(() {
      //判断是否滑动到了页面的最底部
      if (_controller.position.pixels == _controller.position.maxScrollExtent) {
        //如果不是最后一页数据，则生成新的数据添加到list里面
        print("到底了");
      }

      if (fixHeaderKey.currentContext != null) {
        final RenderBox renderBox =
            fixHeaderKey.currentContext.findRenderObject();
        final RenderBox containerRenderBox =
            containerKey.currentContext.findRenderObject();

        // 将父级滚动区域作为祖先，计算该子组件相对于组件的偏移量
        final Offset offset =
            renderBox.localToGlobal(Offset.zero, ancestor: containerRenderBox);

        // 如果偏移量在需要变化的范围内，针对缓慢滚动的情况
        if (offset.dy <= maxScrollHeight && offset.dy >= minScrollHeight) {
          // 外层容器向上的滚动偏移为正数，且滚动的距离为最大滚动高度减去当前子组件的y方向的偏移量
          offsetY = (maxScrollHeight - offset.dy);
        } else {
          // 如果超快速滑动，上面的区间不一定进的去，所以这是针对快速滑动的处理

          //向下滑动超过了maxScrollHeight，并且 offsetY不为0，也就意味外层容器没有向下滚动到期望的位置
          if (offset.dy >= maxScrollHeight && offsetY != minScrollHeight) {
            // 针对向下快速划过的情况
            offsetY = minScrollHeight;
            //向上滑动超过了maxScrollHeight，并且 offsetY不为0，也就意味外层容器没有向上滚动到期望的位置
          } else if (offset.dy <= minScrollHeight &&
              offsetY != maxScrollHeight) {
            // 针对向上快速划过的情况
            offsetY = maxScrollHeight;
          }
        }
        // 通知外部滚动区域进行滚动，隐藏/显示tab栏
        if (lastOffsetY != offsetY) {
          widget.onChangeContainerScrollOffset(offsetY);
          lastOffsetY = offsetY;
        }
      }
    });
    super.initState();
  }

  Widget _buildList() {
    return SliverList(
        delegate: SliverChildBuilderDelegate(
      (context, index) {
        return Column(
          children: [
            Container(
              height: 160,
              color: Colors.red,
              margin: EdgeInsets.only(bottom: 20.w),
            ),
            Container(
              height: 160,
              color: Colors.red,
              margin: EdgeInsets.only(bottom: 20.w),
            ),
            Container(
              height: 260,
              color: Colors.red,
              margin: EdgeInsets.only(bottom: 20.w),
            ),
            Container(
              height: 760,
              color: Colors.red,
              margin: EdgeInsets.only(bottom: 20.w),
            ),
            Container(
              height: 360,
              color: Colors.red,
              margin: EdgeInsets.only(bottom: 20.w),
            )
          ],
        );
      },
      childCount: 1,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      key: containerKey,
      controller: _controller,
      slivers: [
        /// 第一段滚动区域
        SliverToBoxAdapter(
            child: Column(
          children: [
            TopBanner(),
            PopularMovie(),
            Billboard(),
            ComingSoonList(),
            ADArea(),
            MovieOverviewList(),
            PlayList(),
            // 用于滚动定位，无其他意义
            Container(
              key: fixHeaderKey,
            )
          ],
        )),

        /// 固定区域
        SliverPersistentHeader(
          pinned: true, //是否固定在顶部
          floating: true,
          delegate: _SliverAppBarDelegate(
              minHeight: 50, //收起的高度
              maxHeight: 50, //展开的最大高度
              child: Container(
                color: Colors.green,
                alignment: Alignment.centerLeft,
                child: Text("浮动", style: TextStyle(fontSize: 18)),
              )),
        ),

        /// 第二段滚动区域
        _buildList()
      ],
    );
  }

  @override
  void dispose() {
    // TODO: implement

    super.dispose();
  }
}
