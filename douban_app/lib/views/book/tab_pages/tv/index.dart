import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

///TODO:测试手势或者滚动控制外层滚动
class TV extends StatefulWidget {
  TV({Key key}) : super(key: key);

  @override
  _TVState createState() => _TVState();
}

class _TVState extends State<TV> {
  var _controller = ScrollController();
  var inner_item_key = GlobalKey(debugLabel: "itemTwo1_3_key");
  var innercontainerKey = GlobalKey(debugLabel: "innercontainerKey");

  var offsetY = 0.0;

  double maxScrollHeight = 40.0;
  double minScrollHeight = 0.0;

  @override
  void initState() {
    _controller.addListener(() {
      if (inner_item_key.currentContext != null) {
        RenderBox renderBox = inner_item_key.currentContext.findRenderObject();
        RenderBox renderBoxInner =
            innercontainerKey.currentContext.findRenderObject();
        Offset offset =
            renderBox.localToGlobal(Offset.zero, ancestor: renderBoxInner);

        if (offset.dy <= maxScrollHeight && offset.dy >= minScrollHeight) {
          setState(() {
            offsetY = (maxScrollHeight - offset.dy);
          });
        } else {


          if (offset.dy >= maxScrollHeight && offsetY !=0.0) {
            // 针对向下快速划过的情况
            setState(() {
              offsetY = minScrollHeight;
            });
          }else if(offset.dy <= minScrollHeight && offsetY != 40.0){
            // 针对向上快速划过的情况
            setState(() {
              offsetY = maxScrollHeight;
            });
          }
//          print(offset.dy);
        }
        // print('null');
      }else{
        print('null');
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return IndexedStack(children: [
      Positioned(
          top: -offsetY,
          left: 0,
          height: 300,
          width: 300,
          child: Column(children: [
            Container(
              height: 50,
              decoration: BoxDecoration(color: Colors.yellow),
              child: Text("标题，将会被隐藏"),
            ),
            Expanded(
                child: ListView.builder(
                    key: innercontainerKey,
                    controller: _controller,
                    itemCount: 4,
                    itemBuilder: (a, b) {
                      return Container(
                        key: b == 1 ? inner_item_key : Key(b.toString()),
                        height: 220,
                        color: Colors.deepOrange,
                        margin: EdgeInsets.only(bottom: 20.0),
                        child: Text("inner item $b"),
                      );
                    }))
          ]))
    ]);
  }
}
