import 'dart:convert';

import 'package:flutter/cupertino.dart';

class MoviePlayListCardModel {
  final String cardTitle;
  final int total;
  final int focusCount;
  final List<String> coverImgList;
  MoviePlayListCardModel({
    @required this.cardTitle,
    this.total = 0,
    this.focusCount = 0,
    this.coverImgList = const [],
  });

  Map<String, dynamic> toMap() {
    return {
      'cardTitle': cardTitle,
      'total': total,
      'focusCount': focusCount,
      'coverImgList': coverImgList,
    };
  }

  factory MoviePlayListCardModel.fromMap(Map<String, dynamic> map) {
    return MoviePlayListCardModel(
      cardTitle: map['cardTitle'],
      total: map['total'],
      focusCount: map['focusCount'],
      coverImgList: List<String>.from(map['coverImgList']),
    );
  }

  String toJson() => json.encode(toMap());

  factory MoviePlayListCardModel.fromJson(String source) =>
      MoviePlayListCardModel.fromMap(json.decode(source));
}
