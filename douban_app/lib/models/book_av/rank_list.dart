class RankList {
	String index;
	String bookCoverUrl;
	String bookName;
	double ranking;

	RankList({this.index, this.bookCoverUrl, this.bookName, this.ranking});

	factory RankList.fromJson(Map<String, dynamic> json) => RankList(
				index: json['index'] as String,
				bookCoverUrl: json['bookCoverUrl'] as String,
				bookName: json['bookName'] as String,
				ranking: json['ranking'] as double,
			);

	Map<String, dynamic> toJson() => {
				'index': index,
				'bookCoverUrl': bookCoverUrl,
				'bookName': bookName,
				'ranking': ranking,
			};
}
