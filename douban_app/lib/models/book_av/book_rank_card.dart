import 'rank_list.dart';

class BookRankCard {
  String mainTitle;
  String subTitle;
  String backgroundImageUrl;
  List<RankList> rankList;

  BookRankCard({this.mainTitle, this.subTitle, this.backgroundImageUrl, this.rankList});

  factory BookRankCard.fromJson(Map<String, dynamic> json) => BookRankCard(
        mainTitle: json['mainTitle'] as String,
        subTitle: json['subTitle'] as String,
        backgroundImageUrl: json['backgroundImageUrl'] as String,
        rankList: (json['rankList'] as List<dynamic>)
            ?.map((e) =>
                e == null ? null : RankList.fromJson(e as Map<String, dynamic>))
            ?.toList(),
      );

  Map<String, dynamic> toJson() => {
        'mainTitle': mainTitle,
        'subTitle': subTitle,
        'backgroundImageUrl': backgroundImageUrl,
        'rankList': rankList?.map((e) => e?.toJson())?.toList(),
      };
}
