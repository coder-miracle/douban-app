import 'dart:convert';
import 'package:flutter/cupertino.dart';

///
class MovieBaseModel {
  /// 是否已经加入想看
  bool isInterested;

  /// 电影封面地址
  String imgUrl;

  /// 电影名称
  String name;

  /// 电影评分
  double rate;

  MovieBaseModel({
    @required this.name,
    this.isInterested = false,
    this.imgUrl = "",
    this.rate = 0.0,
  });

  Map<String, dynamic> toMap() {
    return {
      'isInterested': isInterested,
      'imgUrl': imgUrl,
      'name': name,
      'rate': rate,
    };
  }

  factory MovieBaseModel.fromMap(Map<String, dynamic> map) {
    return MovieBaseModel(
      isInterested: map['isInterested'],
      imgUrl: map['imgUrl'],
      name: map['name'],
      rate: map['rate'],
    );
  }

  String toJson() => json.encode(toMap());

  factory MovieBaseModel.fromJson(String source) =>
      MovieBaseModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'MovieBaseModel(isInterested: $isInterested, imgUrl: $imgUrl, name: $name, rate: $rate)';
  }
}
