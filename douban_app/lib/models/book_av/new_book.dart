/// bookName : "霸王别姬"
/// coverUrl : "http://ssss"
/// like : 123
/// author : "千叶"
/// tags : ["女性主义","千叶"]
/// describe : "女性主义是什么"

class NewBook {
  NewBook({
      String bookName,
      String coverUrl,
      int like,
      String author,
      List<String> tags,
      String describe,}){
    _bookName = bookName;
    _coverUrl = coverUrl;
    _like = like;
    _author = author;
    _tags = tags;
    _describe = describe;
}

  NewBook.fromJson(dynamic json) {
    _bookName = json['bookName'];
    _coverUrl = json['coverUrl'];
    _like = json['like'];
    _author = json['author'];
    _tags = json['tags'] != null ? json['tags'].cast<String>() : [];
    _describe = json['describe'];
  }
  String _bookName;
  String _coverUrl;
  int _like;
  String _author;
  List<String> _tags;
  String _describe;

  String get bookName => _bookName;
  String get coverUrl => _coverUrl;
  int get like => _like;
  String get author => _author;
  List<String> get tags => _tags;
  String get describe => _describe;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['bookName'] = _bookName;
    map['coverUrl'] = _coverUrl;
    map['like'] = _like;
    map['author'] = _author;
    map['tags'] = _tags;
    map['describe'] = _describe;
    return map;
  }

}