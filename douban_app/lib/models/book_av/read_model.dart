/// rating : ["9.6","50"]
/// rank : 2
/// cover_url : "https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561716440.webp"
/// is_playable : true
/// id : "1291546"
/// types : ["剧情","爱情","同性"]
/// regions : ["中国大陆","中国香港"]
/// title : "霸王别姬"
/// url : "https://movie.douban.com/subject/1291546/"
/// release_date : "1993-07-26"
/// actor_count : 26
/// vote_count : 1835269
/// score : "9.6"
/// actors : ["张国荣","张丰毅","巩俐","葛优","英达","蒋雯丽","吴大维","吕齐","雷汉","尹治","马明威","费振翔","智一桐","李春","赵海龙","李丹","童弟","沈慧芬","黄斐","徐杰","黄磊","冯远征","杨立新","方征","周璞","隋永清"]
/// is_watched : false

class ReadModel {
  ReadModel({
    List<String> rating,
    int rank,
    String coverUrl,
    bool isPlayable,
    String id,
    List<String> types,
    List<String> regions,
    String title,
    String url,
    String releaseDate,
    int actorCount,
    int voteCount,
    String score,
    List<String> actors,
    bool isWatched,
  }) {
    _rating = rating;
    _rank = rank;
    _coverUrl = coverUrl;
    _isPlayable = isPlayable;
    _id = id;
    _types = types;
    _regions = regions;
    _title = title;
    _url = url;
    _releaseDate = releaseDate;
    _actorCount = actorCount;
    _voteCount = voteCount;
    _score = score;
    _actors = actors;
    _isWatched = isWatched;
  }

  ReadModel.fromJson(dynamic json) {
    _rating = json['rating'] != null ? json['rating'].cast<String>() : [];
    _rank = json['rank'];
    _coverUrl = json['cover_url'];
    _isPlayable = json['is_playable'];
    _id = json['id'];
    _types = json['types'] != null ? json['types'].cast<String>() : [];
    _regions = json['regions'] != null ? json['regions'].cast<String>() : [];
    _title = json['title'];
    _url = json['url'];
    _releaseDate = json['release_date'];
    _actorCount = json['actor_count'];
    _voteCount = json['vote_count'];
    _score = json['score'];
    _actors = json['actors'] != null ? json['actors'].cast<String>() : [];
    _isWatched = json['is_watched'];
  }
  List<String> _rating;
  int _rank;
  String _coverUrl;
  bool _isPlayable;
  String _id;
  List<String> _types;
  List<String> _regions;
  String _title;
  String _url;
  String _releaseDate;
  int _actorCount;
  int _voteCount;
  String _score;
  List<String> _actors;
  bool _isWatched;

  List<String> get rating => _rating;
  int get rank => _rank;
  String get coverUrl => _coverUrl;
  bool get isPlayable => _isPlayable;
  String get id => _id;
  List<String> get types => _types;
  List<String> get regions => _regions;
  String get title => _title;
  String get url => _url;
  String get releaseDate => _releaseDate;
  int get actorCount => _actorCount;
  int get voteCount => _voteCount;
  String get score => _score;
  List<String> get actors => _actors;
  bool get isWatched => _isWatched;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['rating'] = _rating;
    map['rank'] = _rank;
    map['cover_url'] = _coverUrl;
    map['is_playable'] = _isPlayable;
    map['id'] = _id;
    map['types'] = _types;
    map['regions'] = _regions;
    map['title'] = _title;
    map['url'] = _url;
    map['release_date'] = _releaseDate;
    map['actor_count'] = _actorCount;
    map['vote_count'] = _voteCount;
    map['score'] = _score;
    map['actors'] = _actors;
    map['is_watched'] = _isWatched;
    return map;
  }
}

