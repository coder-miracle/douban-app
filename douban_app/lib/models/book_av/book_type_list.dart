import 'sub_type_list.dart';

class BookTypeList {
	String typeTitle;
	List<SubTypeList> subTypeList;

	BookTypeList({this.typeTitle, this.subTypeList});

	factory BookTypeList.fromJson(Map<String, dynamic> json) => BookTypeList(
				typeTitle: json['typeTitle'] as String,
				subTypeList: (json['subTypeList'] as List<dynamic>)
						?.map((e) => e == null
								? null
								: SubTypeList.fromJson(e as Map<String, dynamic>))
						?.toList(),
			);

	Map<String, dynamic> toJson() => {
				'typeTitle': typeTitle,
				'subTypeList': subTypeList?.map((e) => e?.toJson())?.toList(),
			};
}
