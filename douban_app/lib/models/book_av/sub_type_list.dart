class SubTypeList {
	String subTypeTitle;
	int color;

	SubTypeList({this.subTypeTitle, this.color});

	factory SubTypeList.fromJson(Map<String, dynamic> json) => SubTypeList(
				subTypeTitle: json['subTypeTitle'] as String,
				color: json['color'] as int,
			);

	Map<String, dynamic> toJson() => {
				'subTypeTitle': subTypeTitle,
				'color': color,
			};
}
