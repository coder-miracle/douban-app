import 'dart:convert';

import 'package:flutter/cupertino.dart';

import 'package:douban_app/models/book_av/movie_ranking_item_model.dart';

class MovieOverviewCardModel {
  /// 卡片标题
  String title;

  /// 卡片副标题
  String subTitle;

  /// 卡片背景图
  String bgImageUrl;

  /// 卡片掩膜颜色
  Color maskColor;

  List<MovieRankingItemModel> movieRakingItem;
  MovieOverviewCardModel({
    @required this.title,
    this.subTitle,
    this.maskColor = const Color(0x7f2234),
    this.bgImageUrl,
    this.movieRakingItem = const [],
  });

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'subTitle': subTitle,
      'bgImageUrl': bgImageUrl,
      'maskColor': maskColor.value,
      'movieRakingItem': movieRakingItem?.map((x) => x.toMap())?.toList(),
    };
  }

  factory MovieOverviewCardModel.fromMap(Map<String, dynamic> map) {
    return MovieOverviewCardModel(
      title: map['title'],
      subTitle: map['subTitle'],
      bgImageUrl: map['bgImageUrl'],
      maskColor: Color(map['maskColor'] is String
          ? int.parse(map['maskColor'])
          : map['maskColor']),
      movieRakingItem: List<MovieRankingItemModel>.from(
          map['movieRakingItem']?.map((x) => MovieRankingItemModel.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory MovieOverviewCardModel.fromJson(String source) =>
      MovieOverviewCardModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'MovieOverviewCardModel(title: $title, subTitle: $subTitle, bgImageUrl: $bgImageUrl, maskColor: $maskColor, movieRakingItem: $movieRakingItem)';
  }
}
