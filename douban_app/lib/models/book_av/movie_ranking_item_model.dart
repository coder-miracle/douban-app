import 'dart:convert';

import 'package:flutter/cupertino.dart';

class MovieRankingItemModel {
  /// 排行
  int ranking;

  /// 排行变化
  int rankingChange;

  /// 详情跳转地址（H5）
  String detailUrl;

  /// 电影封面地址
  String imgUrl;

  /// 电影名称
  String name;

  /// 电影评分
  double rate;
  MovieRankingItemModel({
    this.ranking,
    this.rankingChange,
    this.detailUrl,
    this.imgUrl,
    @required this.name,
    this.rate,
  });

  Map<String, dynamic> toMap() {
    return {
      'ranking': ranking,
      'rankingChange': rankingChange,
      'detailUrl': detailUrl,
      'imgUrl': imgUrl,
      'name': name,
      'rate': rate,
    };
  }

  factory MovieRankingItemModel.fromMap(Map<String, dynamic> map) {
    return MovieRankingItemModel(
      ranking: map['ranking'],
      rankingChange: map['rankingChange'],
      detailUrl: map['detailUrl'],
      imgUrl: map['imgUrl'],
      name: map['name'],
      rate: map['rate'],
    );
  }

  String toJson() => json.encode(toMap());

  factory MovieRankingItemModel.fromJson(String source) =>
      MovieRankingItemModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'MovieRankingItemModel(ranking: $ranking, rankingChange: $rankingChange, detailUrl: $detailUrl, imgUrl: $imgUrl, name: $name, rate: $rate)';
  }
}
