
import 'package:dio/dio.dart';

const timeout = 5000;

class HttpRequest {
  static BaseOptions baseOptions = BaseOptions(connectTimeout: timeout);
  static Dio dio = Dio(baseOptions);

  static Future<T> request<T>(String url, {String method = 'get', Map<String, dynamic> params}) async {
    // 单独相关的设置
    Options options = Options();
    options.method = method;
    // 发送网络请求
    try {
      Response response = await dio.request(url, queryParameters: params, options: options);
      return response.data;
    } on DioError catch(e) {
      throw e;
    }
  }
}