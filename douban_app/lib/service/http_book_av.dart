import 'package:douban_app/service/request.dart';

class BookAvRequest {
  Future getMovieTopList(int start, int count) async {
    final url = "https://movie.douban.com/j/chart/top_list?type=11&interval_id=100%3A90&action=&start=$start&limit=$count";
    final result = await HttpRequest.request(url);
    // List movieList = [];
    print(result);
  }
}